import React, { ChangeEvent, useState } from "react";
import {
  Box,
  Button,
  FormControl,
  FormGroup,
  LinearProgress,
  makeStyles,
  TextField,
  Typography,
} from "@material-ui/core";
import axios from "axios";
import { BASE_URL } from "./Home";
import { useNavigate } from "react-router-dom";

const useStyles = makeStyles({
  container: {
    display: "flex",
    justifyContent: "center",
  },
  form: {
    width: "50%",
    margin: "30px 0",
    "& > div": {
      marginTop: "20px",
    },
  },
});

export interface IValues {
  title: string;
  body: string;
  userId: number;
}

const initialValues: IValues = {
  title: "",
  body: "",
  userId: 1,
};

const CreatePost = () => {
  const classes = useStyles();
  const [values, setValues] = useState(initialValues);
  const [loading, setLoading] = useState(false);
  const navigate = useNavigate();

  const { title, body } = initialValues;

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    setValues((values) => ({
      ...values,
      [event.target.name]: event.target.value,
    }));
  };

  const handleSubmit = async () => {
    setLoading(true);
    await axios.post(BASE_URL, values);
    setLoading(false);
    navigate("/");
  };

  return (
    <Box className={classes.container}>
      {loading ? (
        <LinearProgress />
      ) : (
        <FormGroup className={classes.form}>
          <Typography variant="h4" align="center">
            Create Post
          </Typography>
          <FormControl>
            <TextField
              id="my-input"
              name="title"
              label="Title"
              type="text"
              defaultValue={title}
              variant="outlined"
              onChange={handleChange}
            />
          </FormControl>
          <FormControl>
            <TextField
              id="my-input"
              name="body"
              label="Description"
              type="text"
              defaultValue={body}
              variant="outlined"
              onChange={handleChange}
            />
          </FormControl>
          <FormControl>
            <Button variant="contained" color="primary" onClick={handleSubmit}>
              Add Post
            </Button>
          </FormControl>
        </FormGroup>
      )}
    </Box>
  );
};

export default CreatePost;
