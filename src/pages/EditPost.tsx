import React, { ChangeEvent, useEffect, useState } from "react";
import {
  Box,
  Button,
  FormControl,
  FormGroup,
  LinearProgress,
  makeStyles,
  TextField,
  Typography,
} from "@material-ui/core";
import axios from "axios";
import { BASE_URL } from "./Home";
import { useNavigate, useParams } from "react-router-dom";
import { IValues } from "./CreatePost";

const useStyles = makeStyles({
  container: {
    display: "flex",
    justifyContent: "center",
  },
  form: {
    width: "50%",
    margin: "30px 0",
    "& > div": {
      marginTop: "20px",
    },
  },
});

const initialValues: IValues = {
  title: "",
  body: "",
  userId: 1,
};

const CreatePost = () => {
  const classes = useStyles();
  const [values, setValues] = useState(initialValues);
  const [loading, setLoading] = useState(false);
  const navigate = useNavigate();
  const { id } = useParams();

  const { title, body } = values;

  useEffect(() => {
    getPost();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const getPost = async () => {
    setLoading(true);
    const post = await axios.get(`${BASE_URL}/${id}`);
    setValues(post.data);
    setLoading(false);
  };

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    setValues((values) => ({
      ...values,
      [event.target.name]: event.target.value,
    }));
  };

  const handleSubmit = async () => {
    setLoading(true);
    await axios.put(`${BASE_URL}/${id}`, values);
    setLoading(false);
    navigate("/");
  };

  const handleDelete = async () => {
    setLoading(true);
    await axios.delete(`${BASE_URL}/${id}`);
    setLoading(false);
    navigate("/");
  };

  return (
    <Box className={classes.container}>
      {loading ? (
        <LinearProgress />
      ) : (
        <FormGroup className={classes.form}>
          <Typography variant="h4" align="center">
            Edit Post
          </Typography>
          <FormControl>
            <TextField
              id="my-input"
              name="title"
              label="Title"
              type="text"
              value={title}
              variant="outlined"
              onChange={handleChange}
            />
          </FormControl>
          <FormControl>
            <TextField
              id="my-input"
              name="body"
              label="Description"
              type="text"
              value={body}
              variant="outlined"
              onChange={handleChange}
            />
          </FormControl>
          <FormControl>
            <Button variant="contained" color="primary" onClick={handleSubmit}>
              Update Post
            </Button>
          </FormControl>
          <FormControl>
            <Button variant="contained" color="primary" onClick={handleDelete}>
              Delete Post
            </Button>
          </FormControl>
        </FormGroup>
      )}
    </Box>
  );
};

export default CreatePost;
