import React, { useEffect, useState } from "react";
import {
  Container,
  LinearProgress,
  makeStyles,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
} from "@material-ui/core";
import axios from "axios";
import { Link } from "react-router-dom";

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
  tableContainer: {
    marginBottom: 30,
  },
  heading: {
    margin: "30px 0",
  },
  link: {
    textDecoration: "none",
    fontWeight: 600,
    color: "#3f51b5",
  },
});

export const BASE_URL = "https://jsonplaceholder.typicode.com/posts";

interface IPost {
  id: number;
  body: string;
  title: string;
  userId: number;
}

const Home = () => {
  const classes = useStyles();
  const [posts, setPosts] = useState<IPost[]>([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    getPosts();
  }, []);

  const getPosts = async () => {
    setLoading(true);
    const fetchedPosts = await axios.get(BASE_URL);
    setPosts(fetchedPosts.data);
    setLoading(false);
  };

  return (
    <Container>
      <Typography variant="h4" align="center" className={classes.heading}>
        Posts
      </Typography>
      {loading ? (
        <LinearProgress />
      ) : (
        <TableContainer component={Paper} className={classes.tableContainer}>
          <Table className={classes.table} aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell>Id</TableCell>
                <TableCell align="center">Title</TableCell>
                <TableCell align="center">Description</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {posts.map((post) => (
                <TableRow key={post.id}>
                  <TableCell>{post.id}</TableCell>
                  <TableCell>
                    <Link to={`details/${post.id}`} className={classes.link}>
                      {post.title}
                    </Link>
                  </TableCell>
                  <TableCell>{post.body}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      )}
    </Container>
  );
};

export default Home;
